#Ergoemacs-zsh-keybinds

##Introduction

[Ergoemacs](http://ergoemacs.github.io/ergoemacs-mode/) keyboard shortcuts
for [Zsh](http://www.zsh.org/).

##Usage

Just source in your .zshrc or put to .oh-my-zsh/custom/plugins if using
[oh-my-zsh](https://github.com/robbyrussell/oh-my-zsh).And add plugin
in *plugins* variable in your .zshrc
```shell
plugins=(... my-zsh-function)
```
